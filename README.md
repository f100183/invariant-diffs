## Needed to build `ghc`

* `Cabal`: submodule of `ghc`, tag `ghc-8.2.1-release`
* `haskeline`: submodule of `ghc`, tag `ghc-8.2.1-release`
* `hoopl`: submodule of `ghc`, tag `ghc-8.2.1-release`

## Needed to build `uniplate` and `guarded-rewriting`

* `syb`: version 0.7, cloned from `https://github.com/dreixel/syb`

## Needed to build `lens`

* `cabal-doctest`: version 1.0.2, cloned from `https://github.com/phadej/cabal-doctest`
* `profunctors`: version 5.2.1, cloned from `https://github.com/ekmett/profunctors`
* `kan-extensions`: version 5.0.2, cloned from `https://github.com/ekmett/kan-extensions`
* `lens`: version 4.16, cloned from `https://github.com/ekmett/lens`

## Needed to build `microlens-platform`

Nothing :)

## Needed to build `fclabels`

Nothing :)

## Needed to build `clash-lib`

* `fmlist`: version 0.9, cloned from `https://github.com/sjoerdvisscher/fmlist`
